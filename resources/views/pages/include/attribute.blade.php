<div class="container">
    <div class="col-md-3 left-sidebar">
        <style type="text/css">
            ul.nav.nav-pills.nav-style-li li {
                border: 1px solid #ddd;
                border-radius: 10px;
                margin: 2px 5px;
            }
        </style>
        <h2 style="text-align: center; text-transform: uppercase">Thuộc tính sản phẩm</h2>
        <div class="row">
            <h5 style="text-transform: uppercase; margin-bottom: 10px">Màu sắc</h5>
            <ul class="nav nav-pills nav-style-li nav-pills nav-stacked">
                <li><a href="#">Đen</a></li>
                <li><a href="#">Đỏ</a></li>
                <li><a href="#">Xanh</a></li>
                <li><a href="#">Nâu</a></li>
            </ul>
        </div>
        <div class="row">
            <h5 style="text-transform: uppercase; margin-bottom: 10px">Ram</h5>
            <ul class="nav nav-pills nav-style-li nav-pills nav-stacked">
                <li><a href="#">4GB</a></li>
                <li><a href="#">8GB</a></li>
                <li><a href="#">16GB</a></li>
                <li><a href="#">64GB</a></li>
            </ul>
        </div>
        <div class="row">
            <h5 style="text-transform: uppercase; margin-bottom: 10px">CPU</h5>
            <ul class="nav nav-pills nav-style-li nav-pills nav-stacked">
                <li><a href="#">Core i5</a></li>
                <li><a href="#">Core i7</a></li>
            </ul>
        </div>
        <div class="row">
            <h5 style="text-transform: uppercase; margin-bottom: 10px">Dung lượng chứa</h5>
            <ul class="nav nav-pills nav-style-li nav-pills nav-stacked">
                <li><a href="#">500GB</a></li>
                <li><a href="#">1TB</a></li>
                <li><a href="#">2TB</a></li>
                <li><a href="#">4TB</a></li>
            </ul>
        </div>
        <div class="row">
            <h5 style="text-transform: uppercase; margin-bottom: 10px">Sắp xếp theo </h5>
            <form>
                @csrf
                <select name="sort" id="sort" class="form-control">
                    <option value="{{Request::url()}}?sort_by=none">--Lọc theo--</option>
                    <option value="{{Request::url()}}?sort_by=tang_dan">--Giá tăng dần--</option>
                    <option value="{{Request::url()}}?sort_by=giam_dan">--Giá giảm dần--</option>
                    <option value="{{Request::url()}}?sort_by=kytu_az">Lọc theo tên A đến Z</option>
                    <option value="{{Request::url()}}?sort_by=kytu_za">Lọc theo tên Z đến A</option>
                </select>
            </form>
        </div>
        <div class="row">
            <h5 style="text-transform: uppercase; margin-bottom: 10px">Lọc theo giá</h5>
            <form>
                <div id="slider-range"></div>
                <style type="text/css">
                    .style-range p {
                        float: left;
                        width: 25%;
                    }
                </style>
                <div class="style-range">
                    <p><input type="text" id="amount_start" readonly
                              style="border:0; color:#f6931f; font-weight:bold;"></p>
                    <p><input type="text" id="amount_end" readonly
                              style="border:0; color:#f6931f; font-weight:bold;"></p>
                </div>
                <input type="hidden" name="start_price" id="start_price">
                <input type="hidden" name="end_price" id="end_price">
                <br>
                <div class="clearfix"></div>
                <input type="submit" name="filter_price" value="Lọc giá" class="btn btn-sm btn-default btn-block" style="background: #ccc;">
            </form>
        </div>
    </div>
