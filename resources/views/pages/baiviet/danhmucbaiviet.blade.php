@extends('layout')

@section('sidebar')
    @include('pages.include.sidebar')
@endsection

@section('content')
    <div class="col-md-9">
        <div class="features_items">
            <h2 class="title text-center">{{$meta_title}} </h2>
            <ul class="list_share_social_network">
                <li class="share_social_network">
                    <div class="fb-share-button" data-href="{{$url_canonical}}" data-layout="button_count"
                         data-size="small"><a
                            target="_blank"
                            href="https://www.facebook.com/sharer/sharer.php?u={{$url_canonical}}&amp;src=sdkpreparse"
                            class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
                </li>
                <li class="share_social_network">
                    <div class="zalo-share-button" data-href="{{$url_canonical}}" data-oaid="511599103991120730"
                         data-layout="1"
                         data-color="blue" data-customize=false></div>
                </li>
            </ul>
        </div>
        <div class="product-image-wrapper" style="border: none;">
            @foreach($post_cate as $key => $p)
                <div class="single-products" style="margin:10px 0;padding: 2px">
                    <div class="text-center">
                        <img style="float:left;width:30%;padding: 5px;height: 150px"
                             src="{{asset('public/uploads/post/'.$p->post_image)}}" alt="{{$p->post_slug}}"/>
                        <h4 class="title" style="color:#000; padding: 5px; text-transform: uppercase; font-size: 16px">{{$p->post_title}}</h4>
                        <p>{!!$p->post_desc!!}</p>
                    </div>
                    <div class="text-right">
                        <a href="{{url('/bai-viet/'.$p->post_slug)}}" class="btn btn-default btn-sm">Xem bài
                            viết</a>
                    </div>
                </div>
                <div class="clearfix"></div>
            @endforeach
        </div>
    </div><!--features_items-->
    <ul class="pagination pagination-sm m-t-none m-b-none">
        {!!$post_cate->links()!!}
    </ul>
    <!--/recommended_items-->
    </div>
@endsection
